<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="problems-ogg-split" xml:lang="ca">

  <info>
    <link type="guide" xref="index#problems"/>
    <revision pkgversion="2.2.0" date="2014-04-11" status="candidate"/>
    <revision pkgversion="2.3.5" date="2015-03-26" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>David King</name>
      <email its:translate="no">amigadave@amigadave.com</email>
    </credit>
    <credit type="editor">
      <name>Ferran Roig</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <desc>Divideix un camp d'etiqueta únic en diversos camps en desar fitxers de Ogg i FLAC</desc>
  </info>

  <title>Divideix camps d'etiqueta al desar</title>

  <p>És possible que els fitxers Ogg i FLAC tinguin més d'un <link xref="tag">camp d'etiqueta</link> de cada tipus. Per exemple, una cançó interpretada per dos artistes l'etiqueta pot tenir dos camps d'Artista, un per a cada un. L'<app>EasyTAG</app> llegeix automàticament els camps extres i els combina per mostrar-los com un de sol.</p>

  <p>Moltes aplicacions no poden llegir correctament els camps amb valors múltiples, per això la configuració per defecte desa cada entrada com a un únic camp. Si l'aplicació que utilitzeu pot llegir camps múltiples d'un mateix tipus, podeu configurar l'EasyTAG per què separi els camps quan desi un fitxer. Els camps es separaran automàticament allà on aparegui el caràcter " - ". Per exemple, si el camp conté "David Bowie - Queen", es desaran dos camps d'artista:  "David Bowie" i "Queen".</p>

  <p>Per separar camps quan es desin els fitxers:</p>

  <steps>
    <item>
      <p>Seleccioneu <guiseq style="menuitem"><gui style="menu">Edita</gui><gui style="menuitem">Preferències</gui></guiseq></p>
    </item>
    <item>
      <p>Select the <gui style="tab">Tags</gui> tab</p>
    </item>
    <item>
      <p>In <gui style="group">Splitting</gui> section, check the fields that
      you want to be split into multiple fields when saving</p>
    </item>
  </steps>

</page>
