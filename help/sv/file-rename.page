<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="guide" id="file-rename" xml:lang="sv">

  <info>
    <link type="guide" xref="index#main"/>
    <revision pkgversion="2.1.9" date="2013-08-18" status="draft"/>
    <revision pkgversion="2.3.5" date="2015-03-26" status="draft"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ändra filnamn genom att använda taggar</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marcus Gisslén</mal:name>
      <mal:email>marcus.gisslen@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Byt namn på filer</title>

  <p>Du kan byta namn på dina musikfiler genom att använda taggarna i <app>EasyTAG</app>:</p>

  <steps>
    <item>
      <p><link xref="file-select">Välj filerna</link> som du vill byta namn på i filvisningsläget.</p>
    </item>
    <item>
      <p>Välj <guiseq><gui style="menu">Visa</gui> <gui style="menuitem">Visa avsökare</gui></guiseq>.</p>
    </item>
    <item>
      <p>Välj <gui>Byt namn på fil</gui>-avsökaren.</p>
    </item>
    <item>
      <p>Använd <gui xref="format-specifier">Avsökningssymboler</gui>, skriv in strukturen som du vill använda vid namngivning av filer. T.ex. om du vill att dina filer ska namnges <file>[artist] - [titel].[ändelse]</file>, skriv <input>%a - %t</input> i namngivningsfältet.</p>
    </item>
    <item>
      <p>För att utföra ändringarna på de valda filerna, klicka <gui style="button">Sök av filer</gui>.</p>
    </item>
    <item>
      <p>Stäng <gui style="dialog">Tagg- och filnamnssökning</gui>-dialogrutan.</p>
    </item>
    <item>
      <p>För att spara de utförda ändringarna, välj <guiseq><gui style="menu">Arkiv</gui> <gui style="menuitem">Spara filer</gui></guiseq>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Om du stänger programmet utan att spara kommer du att förlora dina ändringar.</p>
  </note>

</page>
