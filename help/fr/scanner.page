<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="scanner" xml:lang="fr">

  <info>
    <link type="guide" xref="index#main" group="#first"/>
    <revision pkgversion="2.1.9" date="2013-10-06" status="review"/>
    <revision pkgversion="2.3.5" date="2015-03-26" status="review"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mettre à jour automatiquement les étiquettes</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Le <gui>Scanner</gui></title>

  <p>Le scanner peut être utilisé pour remplir les champs en fonction du nom de fichier, renommer un fichier et créer un nouveau répertoire en fonction des champs remplis, et traiter le texte en champs ainsi que le nom de fichier.</p>

<section id="fill-tag">
  <title>Remplissage des champs depuis un nom de fichier et une structure de répertoire</title>

  <p><gui>Compléter les étiquettes</gui> est utilisé pour remplir les champs d'étiquettes avec les données tirées du nom du fichier et de ses répertoire parents.</p>

  <p>Servez-vous des <em xref="format-specifier">spécificateurs de format</em> et des <em>séparateurs</em> pour diviser le nom de fichier et les répertoire parents en différents champs d'étiquette. Les <em>spécificateurs de format</em>, comme <input>%t</input> pour le titre de la piste, utilisés pour indiquer les différents champs d'étiquette, figurent dans la <gui>Légende</gui>. Les <em>séparateurs</em> peuvent être n'importe quelle partie du nom de fichier ou des répertoires parents. Utilisez <key>/</key> pour ajouter le <em>séparateur</em> d'un répertoire parent.</p>

  <p>Par exemple, si vous conservez vos fichiers audio en employant la structure de répertoire et les noms de fichier <file>artiste/album/Piste 01 titre.flac</file>, utilisez la <em>chaîne de formatage</em> <input>%a/%b/%n %t</input> pour extraire le numéro de piste et le titre depuis le nom de fichier, le titre d'album depuis le répertoire parent et l'artiste depuis le répertoire grand-parent.</p>

  <p>Remplissez les champs d'étiquette des fichiers sélectionnés en cliquant sur le bouton <gui style="button">Scanner les fichiers</gui>.</p>

</section>

<section id="rename">
  <title>Changement des noms de fichiers et création de nouveaux répertoires</title>

  <p><gui xref="file-rename">Renommer le fichier</gui> est utilisé pour créer une arborescence de répertoires et mettre à jour les noms de fichiers en se servant des champs d'étiquettes remplis. Si une nouvelle arborescence de répertoires est définie, elle sera créée à l'intérieur du répertoire où se situe actuellement le fichier.</p>

  <p>Par exemple, si vous avez  un fichier étiqueté dans le répertoire <file>Musique</file>, utilisez la <em>chaîne de formatage</em> <input>%a/%b/%n %t</input> pour créer la structure de fichier et le nom de fichier  <file>Musique/artiste/album/Piste 01 titre.flac</file>. Vous aurez un aperçu du projet de nommage au-dessous de la chaîne de formatage.</p>

  <p>Pour préparer les fichiers en vue du déplacement et du changement de nom, cliquez sur le bouton <gui style="button">Scanner les fichiers</gui>.</p>

</section>

<section id="process">
  <title>Traitement en masse des champs d'étiquette et de nom de fichier</title>

  <p><gui>Traiter les champs</gui> est une procédure de recherche et de remplacement sophistiquée qui permet de sélectionner les champs d'étiquettes à traiter. Elle permet aussi de traiter le nom de fichier.</p>

  <p>Précisez quels champs modifier en les sélectionnant dans la section <gui>Champs de l'étiquette</gui>.</p>

  <p>Vous pouvez <gui>Convertir</gui> des caractères pour une recherche et un remplacement simples, changer la casse et ajouter ou supprimer des espaces.</p>

</section>

</page>
