<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="guide" id="playlist-generate" xml:lang="de">

  <info>
    <link type="guide" xref="index#main"/>
    <revision pkgversion="2.1.9" date="2013-10-06" status="draft"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wiedergabeliste aus der Dateiliste erstellen</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@gmail.com</mal:email>
      <mal:years>2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Wiedergabeliste anlegen</title>

  <p>Sie können eine M3U-Wiedergabeliste basierend auf der in <app>EasyTAG</app> angezeigten Dateien zur Wiedergabe mit einem Medienprogramm erstellen:</p>

  <steps>
    <item>
      <p><link xref="file-select">Wählen Sie die Dateien</link>, die Sie zu einer Wiedergabeliste hinzufügen möchten.</p>
    </item>
    <item>
      <p>Wählen Sie <guiseq><gui style="menu">Verschiedenes</gui> <gui style="menuitem">Wiedergabeliste erstellen …</gui></guiseq>.</p>
    </item>
    <item>
      <p>Wählen Sie <gui style="radiobutton">Maske verwenden</gui> und geben Sie eine Maske gemäß der <link xref="format-specifier">Formatangaben</link> an. Alternativ können Sie <gui style="radiobutton">Ordnernamen verwenden</gui> wählen, um die Wiedergabeliste nach dem in der <gui style="group">Auswahl</gui> gewählten Ordner zu benennen.</p>
    </item>
    <item>
      <p>Wählen Sie <gui style="checkbox">Nur ausgewählte Dateien einfügen</gui>, um nur jene Dateien zur Erstellung einer Wiedergabeliste zu verwenden, die in der Dateiliste ausgewählt sind. Wählen Sie dieses Ankreuzfeld ab, um alle angezeigten Dateien zur Wiedergabeliste hinzuzufügen.</p>
    </item>
    <item>
      <p>Wählen Sie <gui style="radiobutton">Relativen Pfad für Dateien in der Wiedergabeliste verwenden</gui>, außer wenn Sie die Wiedergabeliste nur auf diesem Rechner verwenden und keine Audio-Dateien verschieben wollen. In diesem Fall verwenden Sie <gui style="radiobutton">Vollen Pfad für Dateien in der Wiedergabeliste verwenden</gui>.</p>
    </item>
    <item>
      <p>Wählen Sie <gui style="checkbox">Wiedergabeliste im Elternordner erstellen</gui>, wenn Sie die Wiedergabeliste in dem in dem übergeordneten Ordner des in der  gewählten Ordners anlegen wollen. Anderfalls wird die Wiedergabeliste direkt in dem in der <gui style="group">Auswahl</gui> gewählten Ordner angelegt.</p>
    </item>
    <item>
      <p>Wenn Sie eine Wiedergabeliste für einen Windows-Rechner oder für ein <sys>NTFS</sys>- oder <sys>FAT</sys>-Dateisystem erstellen, dann können Sie <gui style="checkbox">DOS-Ordnertrennsymbol verwenden</gui> wählen.</p>
    </item>
    <item>
      <p>Wählen Sie <gui style="radiobutton">Nur Liste der Dateien schreiben</gui>, wenn Sie eine Wiedergabeliste anlegen wollen, die nur eine Liste von Dateien enthält. Mit <gui style="radiobutton">Information schreiben mit: Dateiname</gui> werden auch erweiterte Informationen, wie die Spieldauer der Audio-Datei, in die Wiedergabeliste geschrieben. Wählen Sie <gui style="radiobutton">Information schreiben mit:</gui> und geben Sie mittels Formatbezeichnern eine Maske ein, um erweiterte Informationen in die Wiedergabeliste zu schreiben.</p>
    </item>
    <item>
      <p>Wählen Sie <gui style="button">Speichern</gui>, um die Wiedergabeliste zu erstellen.</p>
    </item>
  </steps>

</page>
