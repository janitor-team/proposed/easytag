<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="format-specifier" xml:lang="fr">

  <info>
    <link type="guide" xref="index#main"/>
    <revision pkgversion="2.1.9" date="2013-08-18" status="candidate"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Signification et utilisation des spécificateurs de format</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Spécificateurs de format</title>

  <p>Les <em>spécificateurs de format</em> sont utilisés pour diviser un nom de fichier en étiquettes et pour nommer un fichier ou une liste de lecture sur la base des étiquettes existantes en utilisant <link xref="scanner">le scanner</link>.</p>

  <table rules="rows" frame="top bottom">
    <thead>
      <tr>
        <td>
          <p>Spécificateur de format</p>
        </td>
        <td>
          <p>Champ étiquette correspondant</p>
        </td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <p its:translate="no">%a</p>
        </td>
        <td>
          <p>Artiste : le ou les artiste(s) créateur(s) de la piste ; ce champ peut concerner un artiste invité aussi bien que l'artiste de l'album</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%z</p>
        </td>
        <td>
          <p>Artiste de l'album</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%b</p>
        </td>
        <td>
          <p>Titre de l'album</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%c</p>
        </td>
        <td>
          <p>Commentaire : champ de commentaire libre</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%p</p>
        </td>
        <td>
          <p>Compositeur : créateur de la piste</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%r</p>
        </td>
        <td>
          <p>Titulaire du copyright : c'est habituellement l'artiste, mais il peut s'agir de la maison de disques</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%d</p>
        </td>
        <td>
          <p>Numéro de disque</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%e</p>
        </td>
        <td>
          <p>Codé par : habituellement la personne qui a codé le fichier, mais ce champ peut aussi être utilisé pour l'application ayant servi à coder le fichier.</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%g</p>
        </td>
        <td>
          <p>Genre</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%i</p>
        </td>
        <td>
          <p>Ignoré : ce spécificateur de format ne peut être utilisé que dans <gui>Compléter l'étiquette</gui> pour spécifier les parties du nom de fichier qui doivent être ignorées</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%l</p>
        </td>
        <td>
          <p>Nombre de pistes : nombre total de pistes sur le média</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%o</p>
        </td>
        <td>
          <p>Artiste d'origine</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%n</p>
        </td>
        <td>
          <p>Numéro de piste</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%t</p>
        </td>
        <td>
          <p>Titre de la piste</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%u</p>
        </td>
        <td>
          <p>URL</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%x</p>
        </td>
        <td>
          <p>Nombre de disques</p>
        </td>
      </tr>
      <tr>
        <td>
          <p its:translate="no">%y</p>
        </td>
        <td>
          <p>Année d'enregistrement ou, parfois, date de publication</p>
        </td>
      </tr>
    </tbody>
  </table>

</page>
