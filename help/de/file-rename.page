<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="guide" id="file-rename" xml:lang="de">

  <info>
    <link type="guide" xref="index#main"/>
    <revision pkgversion="2.1.9" date="2013-08-18" status="draft"/>
    <revision pkgversion="2.3.5" date="2015-03-26" status="draft"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dateinamen mit Hilfe von Metainformationen ändern</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@gmail.com</mal:email>
      <mal:years>2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Dateien umbenennen</title>

  <p>Ändern Sie Ihre Musikdateien mit Hilfe von Metainformationen und <app>EasyTAG</app>:</p>

  <steps>
    <item>
      <p><link xref="file-select">Wahlen Sie die Dateien</link>, die Sie in der Dateiansicht umbenennen möchten.</p>
    </item>
    <item>
      <p>Wählen Sie <guiseq><gui style="menu">Ansicht</gui> <gui style="menuitem">Scanner anzeigen</gui></guiseq>.</p>
    </item>
    <item>
      <p>Wählen Sie den Scanner <gui>Datei umbenennen</gui>.</p>
    </item>
    <item>
      <p>Wählen Sie die gewünschte Dateinamenstruktur basierend auf der <gui xref="format-specifier">Legende</gui>. Zum Beispiel: wenn Sie Ihre Dateien <file>[Künstler] - [Titel].[Endung]</file> nennen möchten, geben Sie <input>%a - %t</input> in das Feld ein.</p>
    </item>
    <item>
      <p>Um die Änderungen auf die gewählten Dateien anzuwenden, klicken Sie auf <gui style="button">Dateien einlesen</gui>.</p>
    </item>
    <item>
      <p>Schließen Sie das Dialogfenster <gui style="dialog">Scan von Metainformationen und Dateinamen</gui></p>
    </item>
    <item>
      <p>Um die angewandten Änderungen zu speichern, wählen Sie <guiseq><gui style="menu">Datei</gui> <gui style="menuitem">Dateien speichern</gui></guiseq>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Wenn Sie die Anwendung ohne Speichern beenden, gehen Ihre Änderungen verloren.</p>
  </note>

</page>
