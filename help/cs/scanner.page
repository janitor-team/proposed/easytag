<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="scanner" xml:lang="cs">

  <info>
    <link type="guide" xref="index#main" group="#first"/>
    <revision pkgversion="2.1.9" date="2013-10-06" status="review"/>
    <revision pkgversion="2.3.5" date="2015-03-26" status="review"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak aktualizovat štítky automaticky.</desc>

  </info>

  <title>Průzkumník</title>

  <p>Průzkumníka můžete využít k vyplnění polí podle názvu souboru, k přejmenování souboru a k vytvoření nové složky na základě vyplněných polí a ke zpracování textu v polích a názvu souboru.</p>

<section id="fill-tag">
  <title>Vyplňování polí z názvu souboru a struktury složek</title>

  <p><gui>Vyplnit štítek</gui> můžete použít k vyplnění polí štítku na základě názvu souboru a jeho rodičovských složek.</p>

  <p>Použijte <em xref="format-specifier">formátovací značky </em> a <em>oddělovače</em> k rozdělení názvu souboru a rodičovských složek do různých polí štítku. <em>Formátovací značky</em>, jako je <input>%t</input> pro název skladby, které se používají k určení různých polí šítku, jsou uvedeny v <gui>legendě</gui>. <em>Oddělovače</em> mohou být libovolné části názvu souboru nebo rodičovských složek. Jako <em>oddělovač</em> rodičovské složky použijte <key>/</key>.</p>

  <p>Když například své zvukové soubory uchováváte ve struktuře složek a s názvy souborů <file>umělec/album/01 název skladby.flac</file>, použijte <em>formátovací řetězec</em> <input>%a/%b/%n %t</input> k získání čísla skladby a názvu skladby z názvu souboru, názvu alba z názvu rodičovské složky a umělce z názvu prarodičovské složky.</p>

  <p>K vyplnění polí štítku u vybraných souborů použijte tlačítko <gui style="button">Prozkoumat soubory</gui>.</p>

</section>

<section id="rename">
  <title>Přejmenování souborů a vytváření nových složek</title>

  <p><gui xref="file-rename">Přejmenovat soubor</gui> můžete použít k vytvoření hierarchie složek a k aktualizaci názvů souborů podle vyplněných polí štítku. V případě, že je zadána nová hierarchie složek, bude vytvořena uvnitř složky, ve které se soubor zrovna nachází.</p>

  <p>Pokud například máte soubor se štítkem ve složce <file>Hudba</file>, můžete použít <em>formátovací řetězec</em> <input>%a/%b/%n %t</input>, aby se vytvořila struktura složek a souborů ve formě <file>Hudba/umělec/album/01 název skladby.flac</file>. Pod zadaným formátovacím řetězcem uvidíte náhled schématu pojmenování.</p>

  <p>Pro přípravu souborů k přesunu a přejmenování, zmáčkněte tlačítko <gui style="button">Prozkoumat soubory</gui>.</p>

</section>

<section id="process">
  <title>Hromadné zpracování polí štítků a názvů souborů</title>

  <p><gui>Zpracovat pole</gui> je sofistikovaná funkce hledání a nahrazování umožňující vybrat, která pole štítku se mají zpracovat. Rovněž umožňuje zpracovat název souboru.</p>

  <p>Určete pole, která se mají změnit, tím, že je vyberte v části <gui>Pole štítku</gui>.</p>

  <p>Můžete <gui>Převést</gui> znaky pomocí jednoduchého vyhledávání a nahrazování, měnit velikost písmen a přidávat nebo odstraňovat mezery.</p>

</section>

</page>
