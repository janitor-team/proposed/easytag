<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="guide" id="cddb-search" xml:lang="de">

  <info>
    <link type="guide" style="2column" xref="index#main"/>
    <revision pkgversion="2.1.9" date="2013-10-07" status="review"/>
    <revision pkgversion="2.3.5" date="2015-03-26" status="review"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Metainformationen mit Hilfe einer CD-Datenbank füllen</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@gmail.com</mal:email>
      <mal:years>2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>CDDB durchsuchen</title>

  <p>Wenn Ihre Musik in Alben organisiert ist, kann <app>EasyTAG</app> Online-Datenbanken mit dem <sys>CDDB</sys>-Protokoll durchsuchen und automatisch einige Metainformationen ausfüllen.</p>

  <steps>
    <item>
      <p><link xref="file-select">Wählen sie alle Dateien</link> aus dem Album, nach dem Sie suchen möchten. Die Sortierung der Dateien muss der Titelliste des Albums entsprechen.</p>
    </item>
    <item>
      <p>Wählen Sie <guiseq><gui style="menu">Verschiedenes</gui> <gui style="menuitem">CDDB-Suche …</gui></guiseq>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui style="button">Anhand ausgewählter Dateien suchen</gui> und warten Sie auf die Ergebnisse.</p>
    </item>
    <item>
      <!-- TODO Split Manual search into a tip? -->
      <p>Wählen Sie ein Album aus der Ergebnisliste, um die Titelinformationen rechts anzuzeigen. Wenn das gewählte Album nicht das gewünschte Album ist, versuchen Sie ein anderes Ergebnis aus der Liste.</p>
      <p>Wenn es keine passenden Ergebnisse zu den gewählten Titeln gibt, geben Sie den Namen des Albums im Suchfeld ein. Klicken Sie anschließend auf <gui style="button">Suchen</gui> und warten Sie auf die Ergebnisse. Danach können Sie die Ergebnisse erneut auf das gewünschte Album überprüfen.</p>
    </item>
    <item>
      <p>Wählen Sie die zu befüllenden Metainformationsfelder als Kombination aus den folgenden Möglichkeiten: <gui style="checkbox">Dateiname</gui>, <gui style="checkbox">Titel</gui>, <gui style="checkbox">Künstler</gui>, <gui style="checkbox">Album</gui>, <gui style="checkbox">Jahr</gui>, <gui style="checkbox">Titelnummer</gui>, <gui style="checkbox">Anzahl der Titel</gui> und <gui style="checkbox">Genre</gui>.</p>
    </item>
    <!-- TODO Run the scanner for each file -->
    <!-- TODO Levenshtein matching -->
    <item>
      <p>Um die Metainformationen der gewählten Dateien mit den Ergebnissen der Suche zu befüllen, klicken Sie auf <gui style="button">Anwenden</gui>.</p>
    </item>
  </steps>

  <p>Wenn Sie nur bestimmte Dateien aktualisieren möchten, können Sie Ihre Auswahl nach erfolgreicher Suche ändern.</p>

</page>
